package Labbuppgifter;

import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Uppgift 1
        class Person {

            private String name;
            private String gender;
            private double salary;

            public Person(String name, double salary, String gender) {
                this.name = name;
                this.gender = gender;
                this.salary = salary;
            }

            public String getName() {
                return name;
            }

            public String getGender() {
                return gender;
            }

            public double getSalary() {
                return salary;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public void setSalary(double salary) {
                this.salary = salary;
            }

            @Override
            public String toString() {
                return "Person{" +
                        "name='" + name + '\'' +
                        ", gender='" + gender + '\'' +
                        ", salary=" + salary +
                        '}';
            }

        }

        List<Person> persons = List.of(
                new Person("Alice", 27000, "Female"),
                new Person("Bengt", 51000, "Male"),
                new Person("Mikael", 34000, "Male"),
                new Person("Ann-sofie", 22000, "Female"),
                new Person("Tilda", 54000, "Female"),
                new Person("Göran", 123000, "Male"),
                new Person("Lena", 47000, "Female"),
                new Person("Stefan", 30000, "male"),
                new Person("Kristina", 78000, "Female"),
                new Person("Gustav", 19000, "Male")
        );

        // 1.1 Snittlön för kvinnor
        System.out.println(
                persons
                        .stream()
                        .filter(person -> person.getGender().equals("Female"))
                        .mapToDouble(Person::getSalary)
                        .summaryStatistics()
                        .getAverage()
        );

        // 1.1 Snittlän för män

        System.out.println(
                persons
                        .stream()
                        .filter(person -> person.getGender().equals("Male"))
                        .mapToDouble(Person::getSalary)
                        .summaryStatistics()
                        .getAverage()
        );

        // 1.2

        persons
                .stream()
                .filter(person -> person.getGender().equals("Male") || person.getGender().equals("Female"))
                .max(Comparator.comparing(Person::getSalary))
                .ifPresent(System.out::println);



        persons
                .stream()
                .filter(person -> person.getGender().equals("Male") || person.getGender().equals("Male"))
                .min(Comparator.comparing(Person::getSalary))
                .ifPresent(System.out::println);

    }
}
